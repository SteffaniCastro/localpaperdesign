function loadSwiper(){

  var thisSwiper = [];

  $('.swiper-container').each(function(i) {
    var this_ID = $(this).attr('id');

    if(this_ID === 'swiper-articles'){
      thisSwiper[i] = new Swiper('.swiper-container#'+this_ID,{
        slidesPerView: 1,
        spaceBetween: 30,
        loop: true,
        // init: false,
        pagination: {
          el: '.swiper-pagination.pagination-articles',
          clickable: true,
        },
        breakpoints: {
          320: {
            slidesPerView: 1,
            spaceBetween: 5,
          },
          360: {
            slidesPerView: 1,
            spaceBetween: 5,
          },
          411: {
            slidesPerView: 1,
            spaceBetween: 5,
          },
          640: {
            slidesPerView: 2,
            spaceBetween: 20,
          },
          768: {
            slidesPerView: 4,
            spaceBetween: 40,
          },
          1024: {
            slidesPerView: 5,
            spaceBetween: 50,
          },
        }
      });
    } else {
      thisSwiper[i] = new Swiper('.swiper-container#'+this_ID, {
        navigation: {
          nextEl: '.swiper-button-next',
          prevEl: '.swiper-button-prev',
        },
      });
    }
    // thisSwiper[i].on('slideChange', function () {
    //   console.log('slide '+i+' changed');
    // });
  });

}

function changeAtiveTab(event,tabID){
    let element = event.target;
    while(element.nodeName !== "A"){
      element = element.parentNode;
    }
    ulElement = element.parentNode.parentNode;
    aElements = ulElement.querySelectorAll("li > a");
    tabContents = document.getElementById("tabs-id").querySelectorAll(".tab-content > div");
    for(let i = 0 ; i < aElements.length; i++){
      aElements[i].classList.remove("text-theme");
      aElements[i].classList.add("text-white");
      tabContents[i].classList.add("hidden");
      tabContents[i].classList.remove("block");
    }
    element.classList.remove("text-white");
    element.classList.add("text-theme");
    document.getElementById(tabID).classList.remove("hidden");
    document.getElementById(tabID).classList.add("block");
    loadSwiper();
}

function loadProgressBar(){

    var el = document.getElementById('graph'); // get canvas

    var options = {
        percent:  el.getAttribute('data-percent') || 25,
        size: el.getAttribute('data-size') || 220,
        lineWidth: el.getAttribute('data-line') || 15,
        rotate: el.getAttribute('data-rotate') || 0
    }

    var canvas = document.createElement('canvas');
    var span = document.createElement('span');
    span.textContent = options.percent + '%';
        
    if (typeof(G_vmlCanvasManager) !== 'undefined') {
        G_vmlCanvasManager.initElement(canvas);
    }

    var ctx = canvas.getContext('2d');
    canvas.width = canvas.height = options.size;

    el.appendChild(span);
    el.appendChild(canvas);

    ctx.translate(options.size / 2, options.size / 2); // change center
    ctx.rotate((-1 / 2 + options.rotate / 180) * Math.PI); // rotate -90 deg

    //imd = ctx.getImageData(0, 0, 240, 240);
    var radius = (options.size - options.lineWidth) / 2;

    var drawCircle = function(color, lineWidth, percent) {
            percent = Math.min(Math.max(0, percent || 1), 1);
            ctx.beginPath();
            ctx.arc(0, 0, radius, 0, Math.PI * 2 * percent, false);
            ctx.strokeStyle = color;
            ctx.lineCap = 'round'; // butt, round or square
            ctx.lineWidth = lineWidth
            ctx.stroke();
    };

    // drawCircle('#69F3B4', options.lineWidth,  '16.84');
   
    
    // drawCircle('#fff', options.lineWidth,  100 / 100);
    // drawCircle('#3DDFCB', options.lineWidth,  16.84 / 100);
    // drawCircle('#59ECBC', options.lineWidth,  20.4/ 100);
    // drawCircle('#3DDFCB', options.lineWidth,  28.21 / 100);
    // drawCircle('#29D6D6', options.lineWidth,  35.99 / 100);
    // drawCircle('#1DD1DC', options.lineWidth,  43.76 / 100);
    // drawCircle('#19CFDE', options.lineWidth,  51.49 / 100);

    var grd = ctx.createRadialGradient(75, 50, 5, 90, 60, 100);
    grd.addColorStop(0, "#3DDFCB");
    grd.addColorStop(1, "#59ECBC");
    grd.addColorStop(2, "#3DDFCB");
    grd.addColorStop(3, "#29D6D6");
    grd.addColorStop(4, "#1DD1DC");
    grd.addColorStop(5, "#19CFDE");
    
    // Fill with gradient
    ctx.fillStyle = grd;
    ctx.fillRect(10, 10, 150, 100);
}

